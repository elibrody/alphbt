#pragma once

#include "Utility.h"

class testApp : public ofBaseApp{

public:
	void setup();
	void update();
	void draw();

	void loadNextLevel();
	void loadFreeLevel();

	void keyPressed(int key);
	void keyReleased(int key);
	//void mouseMoved(int x, int y );
	//void mouseDragged(int x, int y, int button);
	//void mousePressed(int x, int y, int button);
	//void mouseReleased(int x, int y, int button);
	//void windowResized(int w, int h);
	//void dragEvent(ofDragInfo dragInfo);
	//void gotMessage(ofMessage msg);

	class Level* _currentLevel;
	class Level* _nextLevel;

	unsigned long long _transitionTime;
	bool _transitionOut;

	ofSoundPlayer winSound1;
	ofSoundPlayer winSound2;

private:
	ofFbo gridTarget;
};