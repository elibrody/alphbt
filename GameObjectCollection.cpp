#include "GameObjectCollection.h"


GameObjectCollection::GameObjectCollection(Grid* grid) : GameObject(grid)
{
}


GameObjectCollection::~GameObjectCollection(void)
{
	///huh??????????????????????
}

void GameObjectCollection::update() {
	for(GameObjectListIterator it = objectList.begin(); it != objectList.end(); it++) {
		(*it)->update();
	}
}

void GameObjectCollection::draw() {
	for(GameObjectListIterator it = objectList.begin(); it != objectList.end(); it++) {
		(*it)->draw();
	}
}

void GameObjectCollection::addGameObject(GameObject* obj) {
	objectList.push_back(obj);
}

void GameObjectCollection::removeGameObject(GameObject* obj) {
	objectList.erase(remove(objectList.begin(), objectList.end(), obj), objectList.end());
}