#pragma once

#include "Utility.h"

#define LETTER_COUNT 26

class Grid
{
public:
	Grid(int width, int height, int tileSize, bool isFreeMode);
	~Grid(void);

	int tileWidth() {return _width;};
	int tileHeight() {return _height;};
	int tileSize() {return _tileSize;};

	int width() {return _width * _tileSize;}
	int height() {return _height * _tileSize;}

	void addLetterToBank(int code);

	void addAtomicWidget(ofVec2f loc);
	void addObstacle(ofVec2f loc);
	void addGoal(ofVec2f loc);
	void addLetter(int key, ofVec2f location, bool isStatic);

	bool hasWon();

	void tryAddLetterBlock(int key, ofVec2f location);
	void tryRemoveAt(ofVec3f location);

	void setup();
	void draw();
	void drawBank(ofVec2f loc, float width, float height);
	void drawTitle(ofVec2f loc, float width, char* str);
	void update();

	void drawBackground();

	ofTrueTypeFont _c64Font;

	ofSoundPlayer placeBlockSound;
	ofSoundPlayer removeBlockSound;

private:
	int _width, _height, _tileSize;

	int _letterBank[LETTER_COUNT];
	bool _isFreeMode;

	class GameObjectCollection* _blocks;
	class GameObjectCollection* _widgets;
	class GameObjectCollection* _goals;
	class GameObjectCollection* _staticBlocks;
	class GameObjectCollection* _obstacles;
};