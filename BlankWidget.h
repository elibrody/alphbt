#pragma once
#include "widget.h"
class BlankWidget :
	public Widget
{
public:
	BlankWidget(class Grid* grid);
	~BlankWidget(void);

	virtual void draw() {  }
	virtual bool isLocationAvailable(ofVec2f) {return false;}
	virtual bool isThisGoalLocationSatisfied(ofVec2f) {return false;}
};

