#include "Grid.h"
#include "GameObjectCollection.h"
#include "Block.h"
#include "Widget.h"
#include "AtomicWidget.h"
#include "Goal.h"
#include "Obstacle.h"

Grid::Grid(int width, int height, int tileSize, bool isFreeMode) : _width(width), _height(height), _tileSize(tileSize), _isFreeMode(isFreeMode)
{
}


Grid::~Grid(void)
{
	delete _blocks;
	delete _widgets;
	delete _goals;
	delete _staticBlocks;
	delete _obstacles;
}

void Grid::tryRemoveAt(ofVec3f location) {
	//find letter on this location
	Block* block = NULL;
	for(GameObjectListIterator it = _blocks->objectList.begin(); it != _blocks->objectList.end(); it++) {
		if(location.distanceSquared((*it)->location) < EPSILON) {
			block = (Block*)(*it);
		}
	}

	if(block != NULL) {
		removeBlockSound.play();
		_blocks->removeGameObject(block);
		_widgets->removeGameObject(block->getWidget());
		_letterBank[block->_blockType] ++;
	}
}

void Grid::addAtomicWidget(ofVec2f loc) {
	Widget* w = new AtomicWidget(this);
	w->location = loc;
	w->setup();
	_widgets->addGameObject(w);
}


void Grid::tryAddLetterBlock(int key, ofVec2f location) {

	//test we have that letter available
	if(!_isFreeMode && _letterBank[key] <= 0) return;

	//make sure this is not a goal
	for(GameObjectListIterator it = _goals->objectList.begin(); it != _goals->objectList.end(); it++) {
		if(location.distanceSquared((*it)->location) < EPSILON) {
			return;
		}
	}

	//make sure this is not an obstacle
	for(GameObjectListIterator it = _obstacles->objectList.begin(); it != _obstacles->objectList.end(); it++) {
		if(location.distanceSquared((*it)->location) < EPSILON) {
			return;
		}
	}

	//test there is no letter already on that spot
	for(GameObjectListIterator it = _blocks->objectList.begin(); it != _blocks->objectList.end(); it++) {
		if(location.distanceSquared((*it)->location) < EPSILON) {
			return;
		}
	}

	//same for static blocks.
	for(GameObjectListIterator it = _staticBlocks->objectList.begin(); it != _staticBlocks->objectList.end(); it++) {
		if(location.distanceSquared((*it)->location) < EPSILON) {
			return;
		}
	}

	//test an appropriate widget is at that spot
	bool found = false;
	for(GameObjectListIterator it = _widgets->objectList.begin(); it != _widgets->objectList.end(); it++) {
		Widget* w = (Widget*) (*it);
		if(w->isLocationAvailable(location)) {
			found = true;
			break;
		}
	}

	if(!found) {
		return;
	}

	placeBlockSound.play();
	addLetter(key, location, false);

	//remove letter from bank
	_letterBank[key] --;
}

void Grid::addLetter(int key, ofVec2f location, bool isStatic) {
	//create block
	Block* b = new Block(key, this, isStatic);
	b->location = location;
	b->setup();
	
	if(isStatic) {
		_staticBlocks->addGameObject(b);
	} else {
		_blocks->addGameObject(b);
	}

	//add appropriate widget
	Widget *widget = b->getWidget();
	_widgets->addGameObject(widget);
}

bool Grid::hasWon() {
	//ensure all goals are satisfied.

	GameObjectListIterator itg = _goals->objectList.begin();
	bool allSatisfied = true;
	while(itg != _goals->objectList.end() && allSatisfied) {
		GameObjectListIterator itw = _widgets->objectList.begin();
		bool foundSatisfyingThis = false;
		while(itw != _widgets->objectList.end() && !foundSatisfyingThis) {
			Widget* w = (Widget*)(*itw);
			if(w->isThisGoalLocationSatisfied((*itg)->location)) {
				foundSatisfyingThis = true;
			}

			itw++;
		}

		//according to anonymous stack overflow poster, this is fine
		allSatisfied &= foundSatisfyingThis;
		itg++;
	}

	return allSatisfied;
}

void Grid::setup() {

	_c64Font.loadFont("C64_Pro_Mono_v1.0-STYLE.ttf", 64, false, true);

	_blocks = new GameObjectCollection(this);
	_widgets = new GameObjectCollection(this);
	_goals = new GameObjectCollection(this);
	_staticBlocks = new GameObjectCollection(this);
	_obstacles = new GameObjectCollection(this);

	memset(_letterBank, 0, sizeof(_letterBank));

	placeBlockSound.loadSound("place_block.mp3");
	removeBlockSound.loadSound("remove_block.mp3");
}

void Grid::addLetterToBank(int code) {
	_letterBank[code] ++;
}

void Grid::addGoal(ofVec2f loc) {
	Goal* goal = new Goal(this);
	goal->location = loc;
	_goals->addGameObject(goal);
}

void Grid::addObstacle(ofVec2f loc) {
	Obstacle* obst = new Obstacle(this);
	obst->location = loc;
	_obstacles->addGameObject(obst);
}

void Grid::update() {
}

void Grid::drawBackground() {
	ofColor col1(80);
	ofColor col2(col1);
	col2.setBrightness(col2.getBrightness() * 0.9f);

	ofPushMatrix();
		float siz = (float) tileSize();
		ofScale(siz, siz);
		for(int x = 0; x < tileWidth(); x++) {
			for(int y = 0; y < tileHeight(); y++) {
				if((x + y) % 2 == 0) {
					ofSetColor(col1);
				} else {
					ofSetColor(col2);
				}
				ofRect((float)x, (float)y, 1, 1);
			}
		}
	ofPopMatrix();
}

void Grid::draw() {

	drawBackground();

	_widgets->draw();
	_blocks->draw();
	_staticBlocks->draw();
	_obstacles->draw();
	_goals->draw();

}

void Grid::drawTitle(ofVec2f loc, float width, char* str) {
	ofPushMatrix();
		float wid = _c64Font.stringWidth(string(str))/5;
		float left = (width - wid) / 2;
		ofTranslate(loc + ofVec2f(left, -3));

		ofPushMatrix();
			ofScale(1/5.0f, 1/5.0f);
			ofSetColor(ofColor::blue);
			_c64Font.drawString(string(str), 0, 0);
		ofPopMatrix();

	ofPopMatrix();
}

void Grid::drawBank(ofVec2f loc, float width, float height) {
	float margin = 10;

	ofSetColor(ofColor(80));
	ofPushMatrix();
		ofTranslate(loc);
		ofRect(ofVec2f(), width, height);

		float scaleAmount = 1.0f / 5;

		float eachwid = (width - 2*margin) / LETTER_COUNT;
		float topMargin = (height - margin)/2;
		for(int i = 0; i < LETTER_COUNT; i++) {

			int numAvailable = _letterBank[i];

			ofColor col = ofColor(120);
			if(numAvailable > 0 || _isFreeMode) {
				col = ofColor(0, 255, 255);
			}
			ofSetColor(col);

			ofPushMatrix();	
				ofTranslate(ofVec2f(eachwid * i + margin, topMargin));
				ofPushMatrix();
					ofScale(scaleAmount, scaleAmount);
					_c64Font.drawString(string(1, i + 'A'), 0,0);
				ofPopMatrix();
				
				if(numAvailable > 0 || _isFreeMode) {
					ofTranslate(ofVec2f(0, eachwid));
					ofPushMatrix();
						ofScale(scaleAmount, scaleAmount);
						if(_isFreeMode){
							_c64Font.drawString(string("+"), 0,0);
						} else {
							_c64Font.drawString(string(1, numAvailable + '0'), 0,0);
						}
					ofPopMatrix();
				}
			ofPopMatrix();
		}
	ofPopMatrix();
}