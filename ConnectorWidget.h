#pragma once

#include "Widget.h"
#include "Utility.h"

class ConnectorWidget : public Widget
{
public:
	ConnectorWidget(class Grid* grid, ofVec2f origin, ofVec2f target);
	~ConnectorWidget(void);

	ofVec2f _origin, _target;

	virtual void draw();

	virtual bool isLocationAvailable(ofVec2f loc);
	virtual bool isThisGoalLocationSatisfied(ofVec2f loc);
};