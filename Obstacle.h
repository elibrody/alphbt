#pragma once
#include "gameobject.h"
class Obstacle :
	public GameObject
{
public:
	Obstacle(class Grid* grid);
	~Obstacle(void);

	virtual void draw();
};

