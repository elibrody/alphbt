#pragma once
#include "GameObject.h"
#include "Utility.h"

class Block : public GameObject
{
public:
	Block(int blockType, Grid* grid, bool isStatic);

	class Widget* getWidget() { return _widget; }

	~Block(void);

	virtual void setup();
	virtual void draw();

	int _blockType;


private:

	Widget* createConnectorWidget(ofVec2f deltaToTarget);
	Widget* createAuraWidget();

	class Widget* _widget;
	bool _isStatic;
};