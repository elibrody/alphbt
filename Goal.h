#pragma once
#include "GameObject.h"

class Goal : public GameObject
{
public:
	Goal(class Grid* grid);
	~Goal(void);

	virtual void draw();
};


