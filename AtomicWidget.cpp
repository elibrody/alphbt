#include "AtomicWidget.h"
#include "Widget.h"
#include "Grid.h"
#include "Utility.h"

AtomicWidget::AtomicWidget(Grid* grid) : Widget(grid)
{
}


AtomicWidget::~AtomicWidget(void)
{
}

void AtomicWidget::draw() {
	ofPushMatrix();
		ofScale((float)_grid->tileSize(), (float)_grid->tileSize());
		ofTranslate(ofVec2f(0.5f, 0.5f));

		ofSetColor(ofColor(150, 250, 0));
		ofEllipse(location, 0.2f, 0.2f);

	ofPopMatrix();
}

bool AtomicWidget::isLocationAvailable(ofVec2f loc) {
	if(loc.distanceSquared(location) < EPSILON) {
		return true;
	}

	return false;
}

bool AtomicWidget::isThisGoalLocationSatisfied(ofVec2f){
	return false;
}