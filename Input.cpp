#include "Input.h"
#include "Utility.h"

Input& Input::getInstance()
{
	static Input instance; // Guaranteed to be destroyed.
	// Instantiated on first use.
	return instance;
}


Input::Input(void)
{
	//zero keys down.
	memset(keysDown, 0, sizeof(keysDown));
}

Input::~Input(void)
{
}

void Input::update() {
	memset(keysJustDown, 0, sizeof(keysJustDown));

	for(int i = 0; i < NUM_KEYS; i++) {
		bool isDown = ofGetKeyPressed(i);
		if(isDown && !keysDown[i]) {
			keysJustDown[i] = true;
		}

		keysDown[i] = isDown;
	}
}

bool Input::justPressed(int key) {
	return keysJustDown[key];
}