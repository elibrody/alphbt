#include "Player.h"
#include "Input.h"
#include "Utility.h"
#include "Grid.h"

Player::Player(Grid* grid) : GameObject(grid)
{

	move1Sound.loadSound("move1.mp3");
	move2Sound.loadSound("move2.mp3");
	
	move1Sound.setVolume(0.4f);
	move2Sound.setVolume(0.4f);

}


Player::~Player(void)
{
}



void Player::update() {
	Input& input = Input::getInstance();

	ofVec2f delta(0,0);

	if(input.justPressed(OF_KEY_LEFT)) {
		delta.x = -1;
	} else if(input.justPressed(OF_KEY_RIGHT)) {
		delta.x = 1;
	} else if(input.justPressed(OF_KEY_UP)) {
		delta.y = -1;
	} else if(input.justPressed(OF_KEY_DOWN)) {
		delta.y = 1;
	} else if(input.justPressed(OF_KEY_BACKSPACE) || input.justPressed(OF_KEY_DEL)) {
		_grid->tryRemoveAt(location);
	} else {

		//check for letters
		for(int i = 0; i < 26; i++ ){
			if(input.justPressed(i + 'a')) {
				//do something
				_grid->tryAddLetterBlock(i, location);
				break;
			}
		}
	}

	if(delta.distanceSquared(ofVec2f()) > 0) {
		tryMove(delta);
	}
}

void Player::draw() {

	//smoov flashing cursor
	float period = 1.4f;
	float amount = (float) sin(ofGetElapsedTimeMillis() * 2 * PI / (period * 1000));
	ofColor col1 = ofColor(255);
	ofColor col = col1.lerp(ofColor::turquoise, (amount + 1)/2);

	ofSetColor(col);
	float siz = (float) _grid->tileSize();
	float unit = 0.1f;
	ofPushMatrix();
		ofScale(siz, siz);
		ofTranslate(location + ofVec2f(0.5f, 0.5f));
		for(int i = 0; i < 4; i++) {
			ofRotateZ(90);
			ofPushMatrix();
				ofTranslate(ofVec2f(0.5f - unit, 0.5f - unit));
				ofRect(ofVec2f(), unit, unit);
			ofPopMatrix();
		}
	ofPopMatrix();
}

void Player::tryMove(ofVec2f delta) {

	ofVec2f dest = location + delta;
	if(dest.x >=0 && dest.x < _grid->tileWidth()
		&& dest.y >=0 && dest.y < _grid->tileHeight()) {
			if(ofRandom(0,1) > 0.2f) {
				//only play sounds 1/2 the time
				if(ofRandom(0,1) > 0.5f) {
					move1Sound.play();
				} else {
					move2Sound.play();
				}
			}
			location = dest;
	}
}