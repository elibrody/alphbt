#pragma warning(push)
#pragma warning(disable: 820 196 100 668 81 986 619 571 201 625 626 365 514 710 266 265 640 61 305)
	#include "ofMain.h"
	#include "ofAppGlutWindow.h"
#pragma warning(pop)

#pragma warning(disable: 4266 514 710 4505 4640 4820)

#ifdef _DEBUG
//ELI CHEAT: uncomment this line
//adds some CHAET CODES
#define ELI_CHEAT
#endif

#define WIDTH 600
#define MARGIN 20

#define FOOTER_HEIGHT 100

#define HEIGHT (WIDTH + FOOTER_HEIGHT + 4*MARGIN)


#define GRID_WIDTH 7
#define GRID_HEIGHT 7
#define GRID_TILE_SIZE 16

#ifndef STRINGIFY

#define STRINGIFY(A)  #A

#endif


#define EPSILON (0.001f)