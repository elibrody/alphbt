#pragma once

#include "Widget.h"

class AuraWidget : public Widget
{
public:
	AuraWidget(class Grid* grid);
	~AuraWidget(void);

	virtual void draw();
	virtual bool isLocationAvailable(ofVec2f loc);
	virtual bool isThisGoalLocationSatisfied(ofVec2f loc);
};