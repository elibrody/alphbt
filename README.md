ALPHBT by Eli Brody
Created from scratch in under 48 hours for the 26th Ludum Dare game development challenge.

LD entry page:
http://www.ludumdare.com/compo/ludum-dare-26/?action=preview&uid=1395

Minimalist puzzler with a wordy feel. 

Something I dreamed up in microsleep on the first morning of the challenge, so I went with it. 

Built in openFrameworks. Windows only -- a Mac version will appear if I can get my hands on a machine in the next few days. Source freely available on Bitbucket. 

All sounds contributed during the competition by Choochy (age 11 weeks). 

You may need the Microsoft VC runtime for VS2010 
http://www.microsoft.com/en-us/download/details.aspx?id=5555 

Source freely available at: 
https://bitbucket.org/elibrody/alphbt/ 

openFrameworks necessary for compilation: 
http://www.openframeworks.cc/

woo