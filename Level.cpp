#include "Level.h"
#include "Grid.h"
#include "Player.h"

/*
blank level:

0000000
0000000
0000000
0000000
0000000
0000000
0000000

*/

const LevelInfo AllLevels[] = {
		{
		"Welcome!",
		STRINGIFY(
to0play
alphbt:
+place0
0an0"A"
next0to
00the00
goal-@*
),
		"a"
	},
	{
		"Major Energy",
		STRINGIFY(
+000000
0000000
0000000
00@*000
0000000
0000000
0000000
),
		"a"
	},
	{
		"Death Face",
		STRINGIFY(
+000000
0000000
0000*00
0@000*0
0000*00
0000000
bckspce
),
		"abbb"
	},
		{
		"Powder Sail",
		STRINGIFY(
000+000
0000000
000@000
0000000
0*00000
0000000
0000000
),
		"adcc"
	},
			{
		"Generally No Rhyme",
		STRINGIFY(
0000#00
00@0#00
0000#00
+000#00
0000000
00000*0
0000000
),
		"amfgg"
	},
	{
		"Powerful Sunlight",
		STRINGIFY(
000000*
0000000
0000000
0000000
0000000
0@00000
0+00000
),
		"abe"
	},
	{
		"Serious Paper",
		STRINGIFY(
000+000
000000*
0######
0000000
#####00
@000000
0000000
),
		"abbbbbbbbbbeeeeddddd"
	},
	{
		"Finale",
		STRINGIFY(
*00000*
0000000
00the00
00end00
0000000
0000000
*00000*
),
		""
	},
	{
		"Screen Shot Glory",
		STRINGIFY(
+00@00C
####00C
0000A0D
0M0N0Q0
00A000G
000##G0
00*0000
),
		"ashglkbapgnfkflkjfheowbkxjcobxcvbndbd"
	},
		{
		"Screen Shot Glory II",
		STRINGIFY(
*0AAA0*
0000000
+0###00
0A***A0
00###00
0000000
*0AAA0*
),
		"ashglkbapgnfkflkjfheowbkxjcobxcvbndbd"
	},
	{
		"Free Mode",
		STRINGIFY(
+00@000
0000000
0000000
0000000
0000000
0000000
0000000
),
		""
	}
};


Level::Level(LevelInfo info, ofFbo gridTarget, bool isFreeMode) :_info(info), _gridTarget(gridTarget), _isFreeMode(isFreeMode)
{

}


Level::~Level(void)
{
}


void Level::setup() {
	_grid = new Grid(GRID_WIDTH, GRID_HEIGHT, GRID_TILE_SIZE, _isFreeMode);

	if(_isFreeMode) {
		_info = AllLevels[(sizeof(AllLevels) / sizeof(LevelInfo)) - 1 ];
	}

	_grid->setup();

	_player = new Player(_grid);
	_player->setup();

	loadLevel();
}

void Level::loadLevel() {
	float  x = 0, y = 0;
	for(char* it = _info.lvl; *it; ++it) {
		//excluding whitespace???
		if((*it) <= ' ') {
			continue;
		}

		ofVec2f whr = ofVec2f(x, y);

		//add special stuff
		switch((*it)) {
		case '+':
			_player->location = whr;
			break;
		case '@':
			_grid->addAtomicWidget(whr);
			break;
		case '*':
			_grid->addGoal(whr);
			break;
		case '#':
			_grid->addObstacle(whr);
			break;
		case '0':
			//nothing
			break;
		default:
			{
				//a letter
				int code = (*it) - 'A';
				_grid->addLetter(code, whr, true);
			}
			break;
		}

		//move along now
		x++;
		if(x >= GRID_WIDTH) {
			y++;
			x = 0;
		}
	}

	//add bank letters
	for(char* it = _info.letters; *it; ++it) {
		int code = (*it) - 'a';
		_grid->addLetterToBank(code);
	}
}

bool Level::hasWon() {
	if(_isFreeMode) return false;

	return _grid->hasWon();
}

void Level::update() {
	_grid->update();
	_player->update();
}

void Level::draw() {
	ofBackground(150);

	_gridTarget.begin();
		ofBackground(0);
		_grid->draw();
		_player->draw();
	_gridTarget.end();

	ofSetColor(ofColor(255));
	_gridTarget.getTextureReference().setTextureMinMagFilter(GL_NEAREST, GL_NEAREST);  
	_gridTarget.draw(MARGIN, MARGIN, WIDTH - 2*MARGIN, HEIGHT - 3*MARGIN - FOOTER_HEIGHT);

	_grid->drawBank(ofVec2f(MARGIN, HEIGHT - MARGIN - FOOTER_HEIGHT), WIDTH - 2*MARGIN, FOOTER_HEIGHT);

	_grid->drawTitle(ofVec2f(MARGIN, HEIGHT - MARGIN - FOOTER_HEIGHT), WIDTH - 2*MARGIN, _info.title);
}