ALPHBT by Eli Brody
@elibrody
http://blog.megastructure.org/

April, 2013

A wordy-looking puzzler(?) made from scratch for the 26th Ludum Dare 48-hour game development competition.
http://www.ludumdare.com/compo/
Theme: "Minimalism"

Windows only [for now], you may need the Microsoft VC runtime for VS2010?
http://www.microsoft.com/en-us/download/details.aspx?id=5555

Source freely available at:
https://bitbucket.org/elibrody/alphbt/

openFrameworks necessary for compilation:
http://www.openframeworks.cc/

All sounds contributed by Choochy Brody (age 11 weeks)


-------------------------


Thank you for playing!!!!