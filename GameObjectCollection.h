#pragma once

#include "GameObject.h"
#include "Utility.h"

typedef vector<GameObject*> GameObjectList;
typedef GameObjectList::iterator GameObjectListIterator;


class GameObjectCollection : public GameObject
{
public:
	GameObjectCollection(class Grid* grid);
	~GameObjectCollection(void);

	virtual void update();
	virtual void draw();

	void addGameObject(GameObject* obj);
	void removeGameObject(GameObject* obj);

	GameObjectList objectList;
};