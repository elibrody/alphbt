#pragma once

#include "GameObject.h"
#include "Utility.h"

class Player : public GameObject
{
public:
	Player(class Grid* grid);
	~Player(void);

	virtual void update();
	virtual void draw();

	ofSoundPlayer move1Sound, move2Sound;

private:
	void tryMove(ofVec2f delta);
};

