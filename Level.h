#pragma once
#include "Utility.h"

struct LevelInfo {
	char* title;
	char* lvl;
	char* letters;
};

extern const LevelInfo AllLevels[];

class Level
{
public:

	Level(LevelInfo info, ofFbo gridTarget, bool isFreeMode = false);
	~Level(void);

	void setup();
	void update();
	void draw();

	void loadLevel();

	bool hasWon();

	bool  _isFreeMode;

	LevelInfo _info;

	class Grid* _grid;
	class Player* _player;

	ofFbo _gridTarget;
};