#include "Goal.h"
#include "Grid.h"

Goal::Goal(Grid* grid) : GameObject(grid)
{
}


Goal::~Goal(void)
{
}


void Goal::draw() {
	ofPushMatrix();

		float period1 = 6.3f;
		float period2 = 0.4453f;
		float amp1 = 0.8f;
		float amp2 = 1 - amp1;

		float amount = (float) 
				(amp1*sin(ofGetElapsedTimeMillis() * 2 * PI / (period1 * 1000))
			   + amp2*sin(ofGetElapsedTimeMillis() * 2 * PI / (period2 * 1000)));
		ofColor col1 = ofColor(255, 50, 100);
		ofColor col = col1.lerp(ofColor(100, 0, 0), (amount + 1)/2);

		float siz = (float) _grid->tileSize();

		ofScale(siz, siz);
		ofTranslate(location + ofVec2f(0.5f, 0.5f));
		ofSetColor(col);
		ofEllipse(ofVec2f(), 0.6f, 0.6f);
	ofPopMatrix();
}