#include "Obstacle.h"
#include "GameObject.h"
#include "Grid.h"

Obstacle::Obstacle(Grid* grid) : GameObject(grid)
{
}


Obstacle::~Obstacle(void)
{
}

void Obstacle::draw() {
	ofPushMatrix();
		float siz = (float) _grid->tileSize();

		ofScale(siz, siz);
		ofTranslate(location + ofVec2f(0.5f, 0.5f));
		ofSetColor(ofColor(230, 242, 184));
		ofSetRectMode(OF_RECTMODE_CENTER);
		ofRect(ofVec2f(), 0.8f, 0.8f);
		ofSetRectMode(OF_RECTMODE_CORNER);
	ofPopMatrix();
}