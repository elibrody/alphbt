#pragma once

#include "GameObject.h"

class Widget : public GameObject
{
public:
	Widget(class Grid * grid);
	~Widget(void);

	virtual bool isLocationAvailable(ofVec2f loc) = 0;
	virtual bool isThisGoalLocationSatisfied(ofVec2f loc) = 0;
};

