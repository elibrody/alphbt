#pragma once

#include "Utility.h"

#define NUM_KEYS (256 + OF_KEY_MODIFIER)

class Input
{
public:

	static Input& getInstance();

	Input(void);
	~Input(void);

	void update();

	bool justPressed(int key);

private:
	bool keysDown[NUM_KEYS];
	bool keysJustDown[NUM_KEYS];

	Input(Input const&);              // Don't Implement
	void operator=(Input const&); // Don't implement
};