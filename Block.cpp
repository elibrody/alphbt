#include "Block.h"
#include "Grid.h"

#include "Widget.h"
#include "ConnectorWidget.h"
#include "AuraWidget.h"
#include "BlankWidget.h"

Block::Block(int blockType, Grid* grid, bool isStatic) : GameObject(grid), _blockType(blockType), _isStatic(isStatic)
{
}


Block::~Block(void)
{
}

void Block::setup() {
	Widget* w = NULL;
	switch((_blockType + 'A')) {
	case 'A':
		w = createAuraWidget();
		break;
	//linear
	case 'B':
		w = createConnectorWidget(ofVec2f(1,0));
		break;
	case 'C':
		w = createConnectorWidget(ofVec2f(0,1));
		break;
	case 'D':
		w = createConnectorWidget(ofVec2f(-1,0));
		break;
	case 'E':
		w = createConnectorWidget(ofVec2f(0,-1));
		break;
	//x2
	case 'F':
		w = createConnectorWidget(ofVec2f(2,0));
		break;
	case 'G':
		w = createConnectorWidget(ofVec2f(0,2));
		break;
	case 'H':
		w = createConnectorWidget(ofVec2f(-2,0));
		break;
	case 'I':
		w = createConnectorWidget(ofVec2f(0,-2));
		break;

	//diagonals
	case 'J':
		w = createConnectorWidget(ofVec2f(1,1));
		break;
	case 'K':
		w = createConnectorWidget(ofVec2f(-1,1));
		break;
	case 'L':
		w = createConnectorWidget(ofVec2f(-1,-1));
		break;
	case 'M':
		w = createConnectorWidget(ofVec2f(1,-1));
		break;
	//x2 diags
	case 'N':
		w = createConnectorWidget(ofVec2f(1,1)*2);
		break;
	case 'O':
		w = createConnectorWidget(ofVec2f(-1,1)*2);
		break;
	case 'P':
		w = createConnectorWidget(ofVec2f(-1,-1)*2);
		break;
	case 'Q':
		w = createConnectorWidget(ofVec2f(1,-1)*2);
		break;
	//????????????????
	//case 'R':
	//	w = createConnectorWidget(ofVec2f(-1,1));
	//	break;
	//case 'S':
	//	w = createConnectorWidget(ofVec2f(-1,1));
	//	break;
	//case 'T':
	//	w = createConnectorWidget(ofVec2f(-1,1));
	//	break;
	default:
		w = new BlankWidget(_grid);
		break;
	}
	_widget = w;
	_widget->setup();
}

Widget* Block::createAuraWidget() {
	Widget* w = new AuraWidget(_grid);
	w->location = location;
	return w;
}

Widget* Block::createConnectorWidget(ofVec2f deltaToTarget) {
	ofVec2f target = location + deltaToTarget;
	Widget* widget = new ConnectorWidget(_grid, location, target);
	return widget;
}

void Block::draw() {
	ofPushMatrix();

	float siz = (float) _grid->tileSize();
	ofScale(siz, siz);
	ofTranslate(location + ofVec2f(0.15f,0.75f));
	ofColor lettercol = ofColor(255, 120, 100);
	if(_isStatic) {
		lettercol.setBrightness(lettercol.getBrightness() * 0.6f);
	}
	ofSetColor(lettercol);
	ofPushMatrix();
	float fontScale = 1.0f / (15*8);
	ofScale(fontScale, fontScale);
	string str(1, (char) (_blockType + 'A'));
	_grid->_c64Font.drawString(str, 0, 0);
	ofPopMatrix();
	ofPopMatrix();
}