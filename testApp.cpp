#include "testApp.h"

#include "Input.h"
#include "Level.h"
#include "Grid.h"

#define TRANSITION_TIME_IN  (1)
#define TRANSITION_TIME_OUT (3)

//--------------------------------------------------------------
void testApp::setup(){

	_transitionTime = 0;

	winSound1.loadSound("oh_danny.mp3");
	winSound1.setVolume(0.8f);
	winSound2.loadSound("laugh.mp3");


	ofDisableSmoothing();
	ofEnableAlphaBlending();

	//MUCH better performance than not specifying framerate at all!
	ofSetFrameRate(50);

	ofSetWindowTitle("ALPHBT - Eli Brody - LD48 #26");

	gridTarget.allocate(GRID_WIDTH * GRID_TILE_SIZE, GRID_HEIGHT * GRID_TILE_SIZE);

	_levelNum = 0;

	_currentLevel = new Level(AllLevels[_levelNum], gridTarget);
	_currentLevel->setup();
	_nextLevel = NULL;

	//nice fade?
	_transitionOut = false;
	_transitionTime = ofGetElapsedTimeMillis() + TRANSITION_TIME_IN*1000;
}

void testApp::loadFreeLevel(){
	_nextLevel = new Level(AllLevels[5], gridTarget, true);
	_nextLevel->setup();
	_transitionOut = true;
	_transitionTime = ofGetElapsedTimeMillis() + TRANSITION_TIME_OUT*1000;
}

void testApp::loadNextLevel() {
	_nextLevel = new Level(AllLevels[++_levelNum], gridTarget);
	_nextLevel->setup();
	_transitionOut = true;
	_transitionTime = ofGetElapsedTimeMillis() + TRANSITION_TIME_OUT*1000;
}

//--------------------------------------------------------------
void testApp::update(){

	Input::getInstance().update();

	//if transitioning
	if(_nextLevel != NULL) {
		if(_transitionTime <= ofGetElapsedTimeMillis()) {
			//we finished transitioning.
			delete _currentLevel;
			_currentLevel = _nextLevel;
			_nextLevel = NULL;
			
			//and reset transition timeout
			_transitionOut = false;
			_transitionTime = ofGetElapsedTimeMillis() + TRANSITION_TIME_IN*1000;
		}
	} else {
		if(_transitionTime < ofGetElapsedTimeMillis()) {
			_currentLevel->update();

			if(_currentLevel->hasWon()) {
				//play win sound
				if(ofRandom(0,1) < 0.5f) {
					winSound1.play();
				} else {
					winSound2.play();
				}
				loadNextLevel();
			}
		}
	}	
}

//--------------------------------------------------------------
void testApp::draw(){
	_currentLevel->draw();

	if(_transitionTime > 0) {
		if(_transitionTime >= ofGetElapsedTimeMillis() || _transitionOut) {
			float total = TRANSITION_TIME_IN;
			if(_transitionOut) {
				total = TRANSITION_TIME_OUT;
			}

			float alph = (float)(_transitionTime - ofGetElapsedTimeMillis()) / (1000.0f * total);
			alph = ofClamp(alph, 0, 1);
			
			//in or out
			float amount = alph;
			if(_transitionOut) {
				amount = 1 - alph;
			}

			ofColor col = ofColor(255, amount * 255);
			ofSetColor(col);
			ofRect(0,0,(float)ofGetWidth(), (float)ofGetHeight());
		}
	}

#ifdef PRINT_TIMING_DEBUG
	ofSetColor(ofColor::black);

	char numstr[21];
	_itoa_s((int)_transitionTime, numstr, 10);
	string s = string(numstr);
	_itoa_s((int)ofGetElapsedTimeMillis(), numstr, 10);
	s = s + " - " + numstr;
	ofPushMatrix();
	ofScale(1/5.0f, 1/5.f);
	_currentLevel->_grid->_c64Font.drawString(s, 0, 100);
	ofPopMatrix();
#endif
}

//--------------------------------------------------------------
void testApp::keyPressed(int){
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

	if(_transitionTime < ofGetElapsedTimeMillis()) {
#ifdef ELI_CHEAT
	//add level-change cheat
		if(key == '+') {
			loadNextLevel();
		} else if (key == '-') {
			_levelNum -= 2;
			if(_levelNum < -1) { _levelNum = -1; } //no out of bounds on level array
			loadNextLevel();
		}
#endif

		if(key==OF_KEY_F12) {
			loadFreeLevel();
		}
	}
}
//
////--------------------------------------------------------------
//void testApp::mouseMoved(int x, int y ){
//
//}
//
////--------------------------------------------------------------
//void testApp::mouseDragged(int x, int y, int button){
//
//}
//
////--------------------------------------------------------------
//void testApp::mousePressed(int x, int y, int button){
//
//}
//
////--------------------------------------------------------------
//void testApp::mouseReleased(int x, int y, int button){
//
//}
//
////--------------------------------------------------------------
//void testApp::windowResized(int w, int h){
//
//}
//
////--------------------------------------------------------------
//void testApp::gotMessage(ofMessage msg){
//
//}
//
////--------------------------------------------------------------
//void testApp::dragEvent(ofDragInfo dragInfo){ 
//
//}