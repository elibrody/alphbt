#pragma once

#include "Utility.h"

class GameObject
{
public:
	GameObject(class Grid* grid);

	virtual void update();
	virtual void draw();

	virtual void setup();

	virtual ~GameObject(void);

	ofVec2f location;

protected:

	class Grid* _grid;
};