#include "ConnectorWidget.h"
#include "Widget.h"
#include "GameObject.h"
#include "Grid.h"
#include "Utility.h"

ConnectorWidget::ConnectorWidget(class Grid* grid, ofVec2f origin, ofVec2f target)
	: Widget(grid), _origin(origin), _target(target)
{
}


ConnectorWidget::~ConnectorWidget(void)
{
}


void ConnectorWidget::draw() {
	ofPushMatrix();
		ofScale((float)_grid->tileSize(), (float)_grid->tileSize());
		ofTranslate(ofVec2f(0.5f, 0.5f));

		ofSetColor(ofColor(0, 200, 0, 150));
		ofSetLineWidth(1);
		ofLine(_origin, _target);

		ofSetColor(ofColor(0, 200, 0, 255));
		ofEllipse(_target, 0.2f, 0.2f);

	ofPopMatrix();
}


bool ConnectorWidget::isLocationAvailable(ofVec2f loc) {
	if(loc.distanceSquared(_target) < EPSILON) {
		return true;
	}

	return false;
}

bool ConnectorWidget::isThisGoalLocationSatisfied(ofVec2f){
	return false;
}