#pragma once
#include "Widget.h"

class AtomicWidget : public Widget
{
public:
	AtomicWidget(class Grid* grid);
	~AtomicWidget(void);

	virtual void draw();

	virtual bool isLocationAvailable(ofVec2f loc);
	virtual bool isThisGoalLocationSatisfied(ofVec2f loc);
};