#include "AuraWidget.h"
#include "Utility.h"
#include "Grid.h"
#include "Widget.h"


AuraWidget::AuraWidget(Grid* grid) : Widget(grid)
{
}


AuraWidget::~AuraWidget(void)
{
}

void AuraWidget::draw() {

	float rotperiod = 2;
	float amt = (float)(ofGetElapsedTimeMillis() * 2 * PI / (1000 * rotperiod));
	
	float blinkperiod = 0.75f;
	float sinamt = cos((float) (ofGetElapsedTimeMillis() * 2 * PI / (1000 * blinkperiod)));
	ofColor col = ofColor(255, 0, 0).lerp(ofColor(255, 255, 255), sinamt);

	ofPushMatrix();
		ofScale((float)_grid->tileSize(), (float)_grid->tileSize());
		ofTranslate(location + ofVec2f(0.5f, 0.5f));
		ofRotateZ((float)(RAD_TO_DEG * amt));
		ofPushMatrix();
			ofTranslate(ofVec2f(0.5f,0));
			ofSetColor(col);
			ofEllipse(ofVec2f(), 0.2f, 0.2f);
		ofPopMatrix();

	ofPopMatrix();
}

bool AuraWidget::isLocationAvailable(ofVec2f) {
	//nothing doing buster
	return false;
}

bool AuraWidget::isThisGoalLocationSatisfied(ofVec2f loc){
	if(location.distanceSquared(loc) < (1 + EPSILON)) {
		return true;
	}

	return false;
}